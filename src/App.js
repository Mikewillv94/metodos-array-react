import "./App.css";

import React from "react";
import Frutas from "./components/Fruta";

class App extends React.Component {
  fruits = [
    { name: "banana", color: "yellow", price: 2 },
    { name: "cherry", color: "red", price: 3 },
    { name: "strawberry", color: "red", price: 4 },
  ];

  render() {
    return (
      <div className="App">
        <div id="all fruit names">
          <Frutas fruits={this.fruits}></Frutas>
        </div>
        <div id="red fruit names">
          <Frutas
            fruits={this.fruits.filter((cor) => {
              return cor.color === "red";
            })}
          ></Frutas>
        </div>
        <div id="total">
          {this.fruits.reduce((a, { price }) => {
            return a + price;
          }, 0)}
        </div>
      </div>
    );
  }
}

export default App;
