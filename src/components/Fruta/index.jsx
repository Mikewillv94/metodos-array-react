import { Component } from "react";

class Frutas extends Component {
  render() {
    let array = this.props.fruits;
    console.log(array.name);
    return (
      <ul>
        {array.map((fruta, index) => {
          return <li key={index}>{fruta.name} </li>;
        })}
      </ul>
    );
  }
}

export default Frutas;
